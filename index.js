console.log("Activity-24")


// Cube
const num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);



// Address Array
const address = ["258 Washington Ave", "NW", "California", "90011"];
const [street, city, state, zip] = address;
console.log(`I live at ${street}, ${city}, ${state}, ${zip}`);



// Animal Object
const animal = {
	name: "Lolong",
	habitat: "saltwater",
	species: "crocodile",
	weight: "1075 kgs",
	span: "20 ft 3 in.",
};
const { name, habitat, species, weight, span } = animal;
console.log(`${name} was a ${habitat} ${species}. He weighed at ${weight} with a measurement of ${span} `);




// forEach
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => console.log(numbers));



// Reduce
const reducedNumber = numbers.reduce((acc, cur) => acc + cur);
console.log(reducedNumber);




// Dog Class with details
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const theDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(theDog);
